import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:provider_test/main.dart';
import 'package:provider_test/model/pet.dart';
import 'package:provider_test/provider/pets_provider.dart';

import 'lista_pets_test.mocks.dart';

@GenerateMocks([PetsProvider])
void main() {
  group("Testes da lista de itens", () {
    testWidgets("Exibe dois pets com seus nomes na tela inicial", (WidgetTester tester) async {
      MockPetsProvider petsProvider = new MockPetsProvider();
      when(petsProvider.pets).thenReturn([Pet("Bolinha"), Pet("Batata")]);

      // Build our app and trigger a frame.
      Widget app = MultiProvider(
        providers: [
          ChangeNotifierProvider<PetsProvider>(create: (context) => petsProvider)
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: const MyHomePage(title: 'Flutter Demo Home Page'),
        ),
      );

      await tester.pumpWidget(app);

      expect(find.text("Bolinha"), findsOneWidget);
      expect(find.text("Batata"), findsOneWidget);

    });

    testWidgets("Exibe 'nenhum pet encontrado' na tela inicial quando não existir pets", (WidgetTester tester) async {
      MockPetsProvider petsProvider = new MockPetsProvider();
      when(petsProvider.pets).thenReturn([]);
      // Build our app and trigger a frame.
      Widget app = MultiProvider(
        providers: [
          ChangeNotifierProvider<PetsProvider>(create: (context) => petsProvider)
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: const MyHomePage(title: 'Flutter Demo Home Page'),
        ),
      );

      await tester.pumpWidget(app);

      expect(find.text("Bolinha"), findsNothing);
      expect(find.text("Batata"), findsNothing);
      expect(find.text("nenhum pet encontrado"), findsOneWidget);

    });

  });

}